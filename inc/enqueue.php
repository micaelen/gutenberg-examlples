<?php
/* 
 * Enqueue Theme Assets
 *
*/
function enqueue_theme_assets() {
    wp_enqueue_script( 
        'theme-script', 
        get_template_directory_uri() . '/dist/js/theme.min.js', 
        [],
        TRUE
    );

    wp_enqueue_style( 
        'theme-style', 
        get_template_directory_uri() . '/dist/css/theme.min.css',
        [],
        TRUE
    );
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_assets' );