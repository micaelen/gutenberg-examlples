# Gutenberg Theme

Esse repositório contém apenas um tema WordPress simples, criado com blocos Gutenberg customizáveis. 

# Webpack

É usado o [Webpack](https://webpack.js.org/) para compilação de estilos e scripts do tema. Dentro do tema, basta utilizar os seguintes comandos:

`npm install` - instalar as dependências do projeto
`npm run build` - compilar estilos e scripts
