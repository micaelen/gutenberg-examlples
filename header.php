<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;600;700&display=swap" rel="stylesheet">
        <title><?php echo get_bloginfo(); ?></title>

        <?php wp_head(); ?>
    </head>

    <body>
        <header class="header">
            <div class="container">
                <div class="logo">
                    <h1 class="title">Gutenberg</h1>	
                </div>
            </div>
        </header>
        